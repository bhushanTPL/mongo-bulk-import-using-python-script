Prerequisites:
1. Python (v3.6+ or v2.7+)

Steps
1. Clone this repo
2. Cd into repo 'cd mongo-bulk-import-using-python-script'
3. Copy your collections(e.g 'collectionName.json' files) in 'collections' folder
4. enter command 'py mongo-bulk-import.py --db=your-db-name' , change 'your-db-name' to your db where you want make import.

Note: For v2.7 replace 'py' with 'python' in above command.